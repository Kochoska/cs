import codecs
import os
import re
import sys

from setuptools import setup, find_packages


here = os.path.abspath(os.path.dirname(__file__))


def read(*parts):
    # intentionally *not* adding an encoding option to open, See:
    #   https://github.com/pypa/virtualenv/issues/201#issuecomment-3145690
    with codecs.open(os.path.join(here, *parts), 'r') as fp:
        return fp.read()


def find_version(*file_paths):
    version_file = read(*file_paths)
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError("Unable to find version string.")


long_description = read('README.md')


setup(
    name='cs',
    version=find_version("cs", "__init__.py"),
    description = 'Computer Science class',
    packages=['cs'],
    # packages=find_packages(),
    author = 'Szabolcs Blága',
    author_email = 'szabolcs.blaga@gmail.com',
    url = 'https://gitlab.com/blagasz/cs',
    # download_url = 'https://gitlab.com/blagasz/cs/...',
    license = 'GPL',
    include_package_data=True,
    zip_safe=False,
    entry_points = {
        'console_scripts': ['cs=cs:cli'],
    },
    install_requires=[
        'numpy',  
        'scipy',  # remove scipy dependency (norm and spline)
 	    'matplotlib',  # add as optional dependency
        'pandas',  # should be removed from the core (too big)
        'pyyaml',
        'requests',
        'openpyxl',
        'sympy',
        # 'tensorflow',
    ],
    # dependency_links=['http://gitlab.com/blagasz/cs/...'],
    keywords = ['computer science', 'teaching', 'python'],
    classifiers = [],
)
